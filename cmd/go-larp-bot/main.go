package main

import (
	"fmt"
	_ "github.com/gorilla/mux"
	"github.com/urfave/cli/v2"
	"go-larp-bot/pkg/bot"
	"log"
	_ "net/http/pprof"
	"os"
	"time"
)

func main() {
	app := cli.NewApp()
	app.Name = "go-larp-bot"
	app.Usage = "a discord bot for general larping"
	app.Flags = []cli.Flag{
		&cli.BoolFlag{Name: "debug"},
	}
	app.Authors = []*cli.Author{
		{
			Name:  "Evan Schulte",
			Email: "evan.schulte@gmail.com",
		},
	}
	app.Copyright = "(c) 2020 Evan Schulte"
	app.Version = "1.0.0"
	app.Compiled = time.Now()
	app.Commands = []*cli.Command{
		{
			Name:  "bot",
			Usage: "start the bot",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "config", Aliases: []string{"c"}},
			},
			Action: func(c *cli.Context) error {
				cPath := c.String("config")
				b := bot.NewBot(&cPath)
				err := b.Start()
				if err != nil {
					fmt.Printf("error starting bot: %s", err)
				}
				return nil
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
