package bot

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/urfave/cli/v2"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func NewBot(configFile *string) *Bot {
	b := Bot{}
	b.LoadConfig(configFile)
	return &b
}

type Bot struct {
	Session *discordgo.Session `json:"session"`
	Config  *Config            `json:"config"`
}

func (bot *Bot) LoadConfig(path *string) {
	var configFile string
	if path != nil && len(*path) > 0 {
		configFile = *path
	} else {
		configFile = os.Getenv("CONFIGFILE")
	}

	// try to read the config file
	file, err := os.Open(configFile)
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println("failed to close config file after parsing")
		}
	}()

	if os.IsNotExist(err) {
		panic(errors.New(fmt.Sprintln("config json is missing")))
	} else if err != nil {
		panic(err)
	}

	// try to parse the configuration file data into the Configuration struct
	decoder := json.NewDecoder(file)
	bot.Config = &Config{}
	err = decoder.Decode(bot.Config)
	if err != nil {
		panic(err)
	}
}

func (bot *Bot) Start() error {
	if bot.Config == nil || len(bot.Config.Token) == 0 {
		return errors.New("provided client token is invalid")
	}

	var err error
	bot.Session, err = discordgo.New("Bot " + bot.Config.Token)
	if err != nil {
		fmt.Println("Error creating Discord session: ", err)
		return err
	}

	// Register messageCreate as a callback for the messageCreate events.
	bot.Session.AddHandler(bot.MessageCreate)

	err = bot.Session.Open()
	if err != nil {
		fmt.Println("Error opening Discord session: ", err)
		return err
	}

	fmt.Println("LarpBot is now running")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	err = bot.Session.Close()
	if err != nil {
		fmt.Println("Error closing Discord session: ", err)
		return err
	}

	return nil
}

func (bot *Bot) MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	if m.Author.ID == s.State.User.ID {
		return
	}

	bot.Execute(m)
}

func (bot *Bot) getCLI(m *discordgo.MessageCreate) *cli.App {
	app := cli.NewApp()
	app.Name = bot.Config.Command
	app.Flags = []cli.Flag{}
	app.Commands = []*cli.Command{
		{
			Name:    "test",
			Aliases: []string{"p"},
			Usage:   "commands for players",
			Subcommands: []*cli.Command{
				{
					Name:  "get",
					Usage: "test get",
					Flags: []cli.Flag{
						//&cli.StringFlag{Name: "userid", Aliases: []string{"id"}},
						//&cli.StringFlag{Name: "username", Aliases: []string{"un"}},
						//&cli.StringFlag{Name: "nickname", Aliases: []string{"n"}},
					},
					Action: func(c *cli.Context) error {
						return bot.SendMessage(m.ChannelID, "Ethan is a homo")
					},
				},
			},
		},
	}
	return app
}

func (bot *Bot) Execute(m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, bot.Config.Command) {
		// split each line of the message into it's own command
		var commands = strings.Split(m.Content, bot.Config.Command)
		// initialize a new cli app - this is used for easy command parsing
		app := bot.getCLI(m)
		for _, cmd := range commands {
			runCommand := strings.TrimSpace(strings.ReplaceAll(cmd, "\n", " "))
			if len(runCommand) > 0 {
				go func(runCommand string) {
					err := app.Run(strings.Split(fmt.Sprintf("%s %s", bot.Config.Command, runCommand), " "))
					if err != nil {
						fmt.Printf("discord command error: %s", err)
						return
					}
				}(runCommand)
			}
		}
	}
}

func (bot *Bot) SendMessage(channelID string, message string) error {
	_, err := bot.Session.ChannelMessageSend(channelID, message)
	if err != nil {
		return err
	}
	return nil
}
