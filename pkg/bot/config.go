package bot

import "path"

type Config struct {
	Token    string `json:"token"`
	Command  string `json:"command"`
	DataPath string `json:"dataPath"`
}

func (config *Config) GetDataFilePath() string {
	return path.Join(config.DataPath, "data.json")
}
