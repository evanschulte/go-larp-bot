FROM golang:alpine AS build
LABEL maintainer="Evan Schulte <evan.schulte@panteratools.com>"

WORKDIR /go/src/go-larp-bot
COPY . .
RUN go install ./cmd/go-larp-bot

# create a new image with the built binary
FROM alpine:latest
WORKDIR /go/bin
COPY --from=build /go/bin .
COPY /configs .
ENV CONFIGPATH=/go/bin/config.json
ENTRYPOINT ["./go-larp-bot", "bot"]