module go-larp-bot

go 1.13

require (
	github.com/bwmarrin/discordgo v0.20.3
	github.com/gorilla/mux v1.7.4
	github.com/urfave/cli v1.22.4
	github.com/urfave/cli/v2 v2.2.0
)
